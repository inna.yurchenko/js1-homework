// 1. переменная let и const очень похожи, но нельзя поменять значение переменной const (константа). 
// let - изменяемая переменная. переменная var известна во всей функции, в то время как let только в блоке в котором она определена.

// 2. var - устаревшее значение. обрабатывается вначале функции не смотря на то, где она обьявлена. область видимости переменных обьявленых с помощью var больше чем нужно, что делает код неаккуратным.



const name = prompt('What is your name?');
const age = prompt('How old are you?');

if (age < 18) {
    alert('You are not allowed to visit this website.');
} 

if (age >=22){
    alert('Welcome, ' + name);
}

let welcome = confirm('Are you sure you want to continue?');
if (welcome == true) {
    alert('Welcome, ' + name);
} else {
    alert('You are not allowed to visit this website.');
}


